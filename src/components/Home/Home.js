import React, { PropTypes } from 'react'
import {Grid, Row, Col} from 'react-bootstrap'

import './Home.css'

export default class Home extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="container" >
        <Grid>
          <Row className="show-grid" >
            <Col xs={12} md={4}><img src={require('../../public/img/0002.jpg')} width="256px" height="256px" /></Col>
            <Col xs={12} md={8}>
              <h1>自我介紹-余千宸</h1>
              <br/><br/>
              <p className="home-line-hight">
                大家好我叫余千宸，可以叫我<s>千層</s>
                或Anna都可以，很榮幸可以來到這個小組和大家一起工作，
                雖然是個寫網頁的菜逼八新人，但會努力學習der，還請大家多多指教(ゝ∀･)
              </p>
            </Col>
          </Row>
        </Grid>
        <br/>

        <h4>興趣</h4>
        <p className="home-line-hight">
          喜歡打遊戲看動畫漫畫，簡言之就是個宅宅(ﾟ∀。)<br/>
          特別喜歡打音GAME，小到手機上的LoveLive、Deemo大到機台MaiMai和jubeat等等，有興趣可以跟我一起去玩，<s>我最擅長的就是推坑了</s><br/>            常去打桌球和游泳，不過游泳夏天限定冬天太冷了(ry，其他還有喜歡唱歌、看電影、打桌遊等等有機會可以揪團一起去(灬ºωº灬)
        </p>

        <h4> 學術</h4>
        <p className="home-line-hight">
          目前就讀台科大電子系碩士三年級<br/>
          主修是影像處理手勢辨識，機器學習機器視覺等等<br/>
          希望未來可以在這裡學到DeepLearning相關知識，和學習社會業界的經驗
        </p>
      </div>
    )
  }
}
