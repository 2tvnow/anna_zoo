import React, { PropTypes } from 'react'
import {Button, Modal, Table} from 'react-bootstrap'

export default class DetailModal extends React.Component {
  constructor(props) {
    super(props)
  }

  closeDetailModal(){
    this.props.saveDetailModalState(false)
    this.props.handleAnimalTmpChange({validationErrors:{}})
  }

  render() {
    return (
      <Modal animation={false} show={this.props.detailModalState} onHide={this.closeDetailModal.bind(this)}>
        <Modal.Header closeButton>
          <Modal.Title>詳細資料</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table bordered>
            <tbody>
              <tr>
                <th style={{width:"30%"}}>ID</th>
                <th>{this.props.animal.name}</th>
              </tr>
              <tr>
                <th>描述</th>
                <th>{this.props.animal.description}</th>
              </tr>
              <tr>
                <th>性別</th>
                <th>{this.props.animal.sex}</th>
              </tr>
              <tr>
                <th>更新時間</th>
                <th>{this.props.animal.update}</th>
              </tr>
            </tbody>
          </Table>
        </Modal.Body>
      </Modal>
    )
  }
}
