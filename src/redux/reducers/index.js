import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import {createModalState, detailModalState, animal, animals} from './zoo.js'

export default combineReducers({
  zoo: combineReducers({
    createModalState,
    detailModalState,
    animal,
    animals

  }),
  routing: routerReducer
});
