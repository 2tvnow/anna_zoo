import { connect } from 'react-redux'

import Home from '../components/Home/Home.js'
import Zoo from '../components/Zoo.js'

const mapStateToProps = (state, ownProps) => {
  return {
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
